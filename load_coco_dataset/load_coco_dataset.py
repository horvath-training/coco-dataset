from pycocotools.coco import COCO
import tensorflow as tf
import numpy as np
import skimage.io as io
import matplotlib.pyplot as plt
import pylab
import click
import animation
import time
import os

dataDir='../coco'
dataType='val2017'

@animation.wait('spinner')
def init():
  print('\ninitialising...')
  pylab.rcParams['figure.figsize'] = (8.0, 10.0)
  annFile='{}/annotations/instances_{}.json'.format(dataDir,dataType)
  coco = COCO(annFile)
  print('initialising...DONE')
  return coco

@animation.wait('spinner')
def print_info(coco):
  print('\nprinting info...')
  cats = coco.loadCats(coco.getCatIds())
  nms=[cat['name'] for cat in cats]
  nms = set([cat['supercategory'] for cat in cats])
  print('COCO categories: \n{}\n'.format(' '.join(nms)))
  print('COCO supercategories: \n{}'.format(' '.join(nms)))
  print('printing info...DONE')

@animation.wait('spinner')
def load_data(coco, catNms):
  print("\nloading data...")
  data = {}
  data['catIds'] = coco.getCatIds(catNms=catNms)
  data['imgIds'] = coco.getImgIds(catIds=data['catIds'])
  data['cats'] = coco.loadCats(data['catIds'])
  data['images'] = coco.loadImgs(data['imgIds'])
  print("loading data...DONE")
  return data

@animation.wait('spinner')
def save_images(output, images):
  print("\nsaving images...")
  for image in images:
    data = io.imread('%s/images/%s/%s'%(dataDir, dataType, image['file_name']))
    filename = '%s/images/%s.jpg'%(output, image['id'])
    plt.imsave(filename, data)
  print("saving images...DONE")

def int64_feature(value):
  return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))

def int64_list_feature(value):
  return tf.train.Feature(int64_list=tf.train.Int64List(value=value))

def bytes_feature(value):
  return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

def bytes_list_feature(value):
  return tf.train.Feature(bytes_list=tf.train.BytesList(value=value))

def float_list_feature(value):
  return tf.train.Feature(float_list=tf.train.FloatList(value=value))

def create_tf_record(image, annotations_list, image_dir, category_index, include_masks=False):
  """Converts image and annotations to a tf.Example proto.

  Args:
    image: dict with keys:
      [u'license', u'file_name', u'coco_url', u'height', u'width',
      u'date_captured', u'flickr_url', u'id']
    annotations_list:
      list of dicts with keys:
      [u'segmentation', u'area', u'iscrowd', u'image_id',
      u'bbox', u'category_id', u'id']
      Notice that bounding box coordinates in the official COCO dataset are
      given as [x, y, width, height] tuples using absolute coordinates where
      x, y represent the top-left (0-indexed) corner.  This function converts
      to the format expected by the Tensorflow Object Detection API (which is
      which is [ymin, xmin, ymax, xmax] with coordinates normalized relative
      to image size).
    image_dir: directory containing the image files.
    category_index: a dict containing COCO category information keyed
      by the 'id' field of each category.  See the
      label_map_util.create_category_index function.
    include_masks: Whether to include instance segmentations masks
      (PNG encoded) in the result. default: False.
  Returns:
    example: The converted tf.Example
    num_annotations_skipped: Number of (invalid) annotations that were ignored.

  Raises:
    ValueError: if the image pointed to by data['filename'] is not a valid JPEG
  """
  image_height = image['height']
  image_width = image['width']
  filename = '%s.jpg'%(image['id'])
  image_id = image['id']

  full_path = os.path.join(image_dir, filename)
  with tf.gfile.GFile(full_path, 'rb') as fid:
    encoded_jpg = fid.read()
  encoded_jpg_io = io.BytesIO(encoded_jpg)
  image = Image.open(encoded_jpg_io)
  key = hashlib.sha256(encoded_jpg).hexdigest()

  xmin = []
  xmax = []
  ymin = []
  ymax = []
  is_crowd = []
  category_names = []
  category_ids = []
  area = []
  encoded_mask_png = []
  num_annotations_skipped = 0
  for object_annotations in annotations_list:
    (x, y, width, height) = tuple(object_annotations['bbox'])
    if width <= 0 or height <= 0:
      num_annotations_skipped += 1
      continue
    if x + width > image_width or y + height > image_height:
      num_annotations_skipped += 1
      continue
    xmin.append(float(x) / image_width)
    xmax.append(float(x + width) / image_width)
    ymin.append(float(y) / image_height)
    ymax.append(float(y + height) / image_height)
    is_crowd.append(object_annotations['iscrowd'])
    category_id = int(object_annotations['category_id'])
    category_ids.append(category_id)
    category_names.append(category_index[category_id]['name'].encode('utf8'))
    area.append(object_annotations['area'])

    if include_masks:
      run_len_encoding = mask.frPyObjects(object_annotations['segmentation'], image_height, image_width)
      binary_mask = mask.decode(run_len_encoding)
      if not object_annotations['iscrowd']:
        binary_mask = np.amax(binary_mask, axis=2)
      pil_image = PIL.Image.fromarray(binary_mask)
      output_io = io.BytesIO()
      pil_image.save(output_io, format='PNG')
      encoded_mask_png.append(output_io.getvalue())
  feature_dict = {
      'image/height': int64_feature(image_height),
      'image/width': int64_feature(image_width),
      'image/filename': bytes_feature(filename.encode('utf8')),
      'image/source_id': bytes_feature(str(image_id).encode('utf8')),
      'image/key/sha256': bytes_feature(key.encode('utf8')),
      'image/encoded': bytes_feature(encoded_jpg),
      'image/format': bytes_feature('jpeg'.encode('utf8')),
      'image/object/bbox/xmin': float_list_feature(xmin),
      'image/object/bbox/xmax': float_list_feature(xmax),
      'image/object/bbox/ymin': float_list_feature(ymin),
      'image/object/bbox/ymax': float_list_feature(ymax),
      'image/object/class/text': bytes_list_feature(category_names),
      'image/object/is_crowd': int64_list_feature(is_crowd),
      'image/object/area': float_list_feature(area),
  }
  if include_masks:
    feature_dict['image/object/mask'] = (bytes_list_feature(encoded_mask_png))
  example = tf.train.Example(features=tf.train.Features(feature=feature_dict))
  return key, example, num_annotations_skipped

def create_records(output, data, coco):
  records = []
  for image in data['images']:
    annIds = coco.getAnnIds(imgIds=[image['id']], catIds=data['catIds'])
    records.append(
      create_tf_record(image, coco.loadAnns(annIds), output+'/images', data["cats"])
    )
  print(records)

@click.command()
@click.option('--info', is_flag=True, help='display categories')
@click.option('--output', '-o', default="../output", help='output directory')
@click.option('--categories', help='categories to load, to enter multiple categories seperate with ",": "car,person,dog"')
def main(info, output, categories):
  coco = init()
  if info:
    print_info(coco)
  if categories:
    data = load_data(coco, categories.split(','))
    save_images(output, data["images"])
    create_records(output, data, coco)
  
if __name__ == '__main__':
  main()
